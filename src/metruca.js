(function(window){
    function define_metruga () {
        Mustache = require('mustache');
        Metruga = function() {};

        Metruga.prototype.inspect = window.console;
        Metruga.prototype.fn = Metruga.prototype;

        Metruga.prototype.select = function(selector){
            if(selector !== undefined){
                if (typeof selector === "string") {
                    this.element = document.querySelectorAll(selector);
                } else if (selector.length) {
                    this.element = selector;
                } else {
                    this.element = [selector];
                }
            }
            return this;
        };

        Metruga.prototype.date = function(params){
            if(params){
                return new Date(params);
            }
            return new Date;
        };

        Metruga.prototype.extend = function(obj, name) {
           for (var i in obj) {
              if (obj.hasOwnProperty(i)) {
                if(name !== undefined){
                    this[name][i] = obj[i];
                }else{
                    this[i] = obj[i];
                }
              }
           }
           return this;
        };

        Metruga.prototype.map = function (callback) {
            var results = [], i = 0;
            for ( ; i < this.element.length; i++) {
                results.push(callback.call(this.element, this.element[i], i));
            }
            return results;
        };

        Metruga.prototype.mapOne = function (callback) {
            var m = this.map(callback);
            return m.length > 1 ? m : m[0];
        };

        Metruga.prototype.forEach = function(callback) {
            this.map(callback);
            return this;
        };
        
        Metruga.prototype.html = function (html) {
            if (typeof html !== "undefined") {
                this.forEach(function (el) {
                    el.innerHTML = html;
                });
                return this;
            } else {
                return this.mapOne(function (el) {
                    return el.innerHTML;
                });
            }
        };

        Metruga.prototype.child = function () {
            return this.mapOne(function (el) {
                return el.children[0];
            });
        };

        Metruga.prototype.parent = function () {
            return this.mapOne(function (el) {
                return el.children[0];
            });
        };

        Metruga.prototype.ajax = function(options){
            var _xhttp, _options, _url, _good, _type, _bad;

            _type = "GET";
            _url = "";
            _async = true;
            _options = Metruga.extend(options);

            if (window.XMLHttpRequest) {
                _xhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                _xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }

            
            _xhttp.onreadystatechange = function() {
                if (_xhttp.readyState == 4 && _xhttp.status == 200) {
                    this.status = _xhttp.status;
                    this.readyState = _xhttp.readyState;
                    this.xhttp = _xhttp;
                    this.content = _xhttp.responseText;

                    if(options.good !== undefined && typeof options.good === "function"){
                        _good = _options.good;
                        _good(this.content);
                    }

                    if(options.async !== undefined && typeof options.good === "boolean"){
                        _async = _options.async;
                    }
                }else{
                    if(options.bad !== undefined && typeof options.bad === "function"){
                        _bad = _options.bad;
                        _bad();
                    }
                }
            };
            
            
            if(options.type !== undefined && typeof options.type === "string"){
                _type = _options.type;
            }

            if(options.url !== undefined && typeof options.url === "string"){
                _url = _options.url;
            }
            
            _xhttp.open(_type, _url, _async);
            _xhttp.send();

            return _xhttp;
        };

        Metruga.prototype.addEventListener = function(element,event,handler){
            if(document.addEventListener) {
                return function(element, event, handler) {
                    element.addEventListener(event, handler, false);
                };
            }
            else {
                return function(element, event, handler) {
                    element.attachEvent('on' + event, handler);
                };
            }
        };

        Metruga.prototype.path = function(){
            return window.location.hash.split('#')[1];
        };
        Metruga.prototype.hashToArray =  function(){
            return Metruga.hash.split("/");
        };

        Metruga.prototype.hashChange =  function(func){
            return window.addEventListener("hashchange", function (e) {
                if(typeof func === "function"){
                    func();
                }
            }, false);
        };

        Metruga.prototype.navigationHandler = function(){
            var _nav = this.__navigation;
            var _root = this.__root;
            var _error = this.__error;

            if("onhashchange" in window) {
                window.onhashchange = function(){
                   if(typeof _nav === "object" && Metruga.path() !== undefined){
                        for(var i in _nav){
                            if(Metruga.path() === _nav[i].path){
                                if(typeof _nav[i].fn === "function"){
                                    func = _nav[i].fn;
                                    func();
                                }
                                if(typeof _nav[i].render === "object"){
                                    template = _nav[i].render.template;
                                    params = _nav[i].render.template;

                                    Metruga.render(template, params);
                                }
                            }
                        }
                    }else{
                        if(typeof _root.fn === "function"){
                            func = _root.fn;
                            func();
                        }
                    }
                }
            }
        };

        Metruga.prototype.socket = function(url, options){
            var _url, _socket, _options, _open, _message, _close;
            _url = "";
            _options = this.extend(options);


            if ("WebSocket" in window){
                if(typeof _url === "string"){
                    _socket = new WebSocket();
                        
                    _socket.onopen = function(){
                        if(typeof _options.open == "function"){
                            _open = options.open;
                            _open();
                        }
                    };

                    _socket.onmessage = function (event){ 
                        if(typeof _options.message == "function"){
                            _message = options.message;
                            _message(event);
                        }
                    };

                    _socket.onclose = function(){
                        if(typeof _options.close == "function"){
                            _close = options.close;
                            _close();
                        }
                    };
                }else{
                    this.inspect.error("Your url doesn't string.");
                }
            }else{
                this.inspect.error("Your browser doesn't support this function.");
            }

            return _socket;
        };

        Metruga.prototype.getObjectClass = function(obj){
           if (typeof obj != "object" || obj === null) return false;
           else return /(\w+)\(/.exec(obj.constructor.toString())[1];
        }

        Metruga.prototype.render = function(template, params){
            temp = Metruga.select(template).html();
            Mustache.parse(temp);
            rendered = Mustache.render(temp, params);
            Metruga.select(template).html(rendered);
        }; 

        Metruga.prototype.scriptLoader = function(urls){
            for(var i in urls){
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = urls[i];    
                document.getElementsByTagName('head')[0].appendChild(script);
            }
        };

        Metruga.prototype.navigation = function(selector, nav, options){
            this.__navigation = nav;
            selected = Metruga.select(selector);
            this.options = options;

            if(typeof this.options.root === "object"){
                this.__root = this.options.root;
            }
            parent = document.createElement('ul');
            if(typeof this.options.parent === "string"){
                parent = document.createElement(this.options.parent);
            }
            if(typeof this.options.class === "string"){
                parent.className = this.options.class;
            }
            if(typeof this.options.id === "string"){
                parent.id = this.options.id;
            }
            if(typeof nav === "object"){
                for(var element in nav) { 
                    child = document.createElement('li');
                    if(typeof this.options.child === "string"){
                        child = document.createElement(this.options.child);
                    }
                    if(typeof nav[element].class === "string"){
                        child.className = nav[element].class;
                    }
                    if(typeof nav[element].id === "string"){
                        child.id = nav[element].id;
                    }
                    if(typeof nav[element].link === "object"){
                        a = document.createElement('a');
                        a.href = "#"+nav[element].path;
                        if(typeof nav[element].link.class === "string"){
                            a.className = nav[element].link.class;
                        }
                        if(typeof nav[element].link.id === "string"){
                            a.id = nav[element].link.id;
                        }
                        if(typeof nav[element].link.text === "string"){
                            a.innerHTML = nav[element].link.text;
                        }
                        child.innerHTML = a.outerHTML;
                    }else{
                        if(typeof nav[element].content === "string"){
                            child.href = "#"+nav[element].path;
                            child.innerHTML = nav[element].content;
                        }
                    }
                    parent.innerHTML = parent.innerHTML + child.outerHTML;
                }
                selected.html(parent.outerHTML);
                Metruga.navigationHandler();
                
            }
        };

        return new Metruga;
    }

    if(typeof(Metruga) === 'undefined' && typeof(m) === 'undefined'){
        window.m = window.Metruga = define_metruga();
    }
    else{
        console.log("Library already defined.");
    }
})(window);