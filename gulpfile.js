var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
var include = require('gulp-include');

gulp.task('default', function() {
    gulp.src(['./src/*.js'])
        .pipe(include())
        .pipe(browserify({insertGlobals: true }))
        .pipe(uglify())
        .pipe(concat('metruga.min.js'))
        .pipe(gulp.dest('./build/'))
        .pipe(gulp.dest('./example/js/'));


    gulp.src(['./src/*.js'])
        .pipe(include())
        .pipe(browserify({insertGlobals: true }))
        .pipe(concat('metruga.js'))
        .pipe(gulp.dest('./build/'))
        .pipe(gulp.dest('./example/js/'));
});

gulp.task('watch', function() {
    gulp.watch('./lib/*.js', ['default']);
});